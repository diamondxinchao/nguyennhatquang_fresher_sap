USE master
GO
DROP DATABASE IF EXISTS SAP_Test_Tech
GO
CREATE DATABASE SAP_Test_Tech
GO
Use SAP_Test_Tech
GO
CREATE TABLE Customers
(
    Customer_number INT PRIMARY KEY IDENTITY(1,1),
    Customer_name NVARCHAR(50)
)
GO
CREATE TABLE Orders
(
    Order_number int PRIMARY KEY IDENTITY(1,1),
    Customer_number INT,
    Order_quantity INT CHECK(Order_quantity > 0),
    FOREIGN KEY (Customer_number) REFERENCES Customers(Customer_number)
)
GO
INSERT INTO Customers
    (Customer_name)
VALUES
    (N'Nguyen Van A'),
    (N'Company B'),
    ( N'LE Van C'),
    (N'Company D'),
    (N'Company E'),
    (N'Tran Thi P')
GO
SELECT *
FROM Customers
GO
INSERT INTO Orders
    (Customer_number, Order_quantity)
VALUES
    (1, 100),
    (2, 1000),
    (4, 5000),
    (2, 2500),
    (4, 6000),
    (3, 800),
    (5, 10000),
    (6, 921)
GO
SELECT * FROM Orders

SELECT TOP 3 Customer_number,SUM(Order_quantity) AS 'Sum_Order_quantity'
FROM Orders
GROUP BY Customer_number
ORDER BY SUM(Order_quantity) DESC
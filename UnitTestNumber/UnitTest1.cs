using ConvertNumberToCharacter;

namespace UnitTestNumber;

using Xunit;

public class Tests
{
    [Fact]
    public void Unit_Number_ShouldReturnRightStringWithNumber_WhenParamIsZero()
    {
        var number = new Number();
        var param = 0;
        var expected = "không";
        var actual = number.ConvertNumberToCharacter(param);

        Assert.Equal(expected, actual);
    }

    [Fact]
    public void UnitNumberOfTens_ShouldReturnRightStringWithNumber_WhenParamIsTen()
    {
        var number = new Number();
        var param = 10;
        var expected = "mười";
        var actual = number.ConvertNumberToCharacter(param);

        Assert.Equal(expected, actual);
    }

    [Fact]
    public void UnitNumberOfTens_ShouldReturnRightStringWithNumber_WhenParamIsNineteen()
    {
        var number = new Number();
        var param = 19;
        var expected = "mười chín";
        var actual = number.ConvertNumberToCharacter(param);

        Assert.Equal(expected, actual);
    }

    [Fact]
    public void UnitNumberOfHundreds_ShouldReturnRightStringWithNumber_WhenParamIsOneHundredsOne()
    {
        var number = new Number();
        var param = 101;
        var expected = "một trăm linh một";
        var actual = number.ConvertNumberToCharacter(param);

        Assert.Equal(expected, actual);
    }
    [Fact]
    public void UnitNumberOfHundreds_ShouldReturnRightStringWithNumber_WhenParamIsTwoHundredsThirtyOne()
    {
        var number = new Number();
        var param = 231;
        var expected = "hai trăm ba mươi mốt";
        var actual = number.ConvertNumberToCharacter(param);

        Assert.Equal(expected, actual);
    }
}
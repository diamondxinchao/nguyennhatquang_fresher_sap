﻿using ConvertNumberToCharacter;

Console.OutputEncoding = System.Text.Encoding.UTF8;

var number = new Number();
try
{
    Console.WriteLine("Test case for test");
    Console.WriteLine(number.ConvertNumberToCharacter(0));
    Console.WriteLine(number.ConvertNumberToCharacter(10));
    Console.WriteLine(number.ConvertNumberToCharacter(19));
    Console.WriteLine(number.ConvertNumberToCharacter(101));
    Console.WriteLine(number.ConvertNumberToCharacter(231));
    Console.WriteLine("----------------------");
    Console.WriteLine("My test case");
    Console.WriteLine(number.ConvertNumberToCharacter(9));
    Console.WriteLine(number.ConvertNumberToCharacter(10));
    Console.WriteLine(number.ConvertNumberToCharacter(11));
    Console.WriteLine(number.ConvertNumberToCharacter(21));
    Console.WriteLine(number.ConvertNumberToCharacter(35));
    Console.WriteLine(number.ConvertNumberToCharacter(100));
    Console.WriteLine(number.ConvertNumberToCharacter(101));
    Console.WriteLine(number.ConvertNumberToCharacter(105));
    Console.WriteLine(number.ConvertNumberToCharacter(111));
    Console.WriteLine(number.ConvertNumberToCharacter(110));
    Console.WriteLine(number.ConvertNumberToCharacter(505));
    Console.WriteLine(number.ConvertNumberToCharacter(503));
    Console.WriteLine(number.ConvertNumberToCharacter(121));
    Console.WriteLine(number.ConvertNumberToCharacter(1211));
}
catch (Exception e)
{
    Console.WriteLine($"Error: {e.Message}");
}
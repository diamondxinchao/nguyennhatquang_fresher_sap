namespace ConvertNumberToCharacter;

public class Number
{
    private const string UNIT_OF_TENS = "mươi";
    private const string NUMBER_TEN = "mười";
    private const string NUMBER_FIVE = "lăm";
    private const string NUMBER_ONE = "mốt";
    private const string NUMBER_ZERO_IN_HUNDREDS = "linh";
    private const string UNIT_OF_HUNDREDS = "trăm";


    public string ConvertNumberToCharacter(int number)
    {
        
        var convertNumber = number.ToString();
        int lenghtOfNumber = convertNumber.Length;
        switch (lenghtOfNumber)
        {
            case 1:
                return UnitNumber(number);
                break;
            case 2:
                return UnitOfTens(number);
                break;
            case 3:
                 return UnitOfHundreds(number);
                break;
            default:
                throw new Exception("Number must less than one thousand");
                break;
        }
    }

    private string UnitOfHundreds(int number)
    {
        var character = number.ToString();
        var firstUnit = GetNumberInCharacter(character, 0);
        var secondUnit = GetNumberInCharacter(character, 1);
        var thirdUnit = GetNumberInCharacter(character, 2);
        switch (character[1])
        {
            case '0':
                if (thirdUnit == 0)
                {
                    return $"{UnitNumber(firstUnit)} {UNIT_OF_HUNDREDS}";
                }

                return $"{UnitNumber(firstUnit)} {UNIT_OF_HUNDREDS} {NUMBER_ZERO_IN_HUNDREDS} {UnitNumber(thirdUnit)}";
            case '1':
                if (thirdUnit == 0)
                {
                    return $"{UnitNumber(firstUnit)} {UNIT_OF_HUNDREDS} {UNIT_OF_TENS}";
                }

                if (thirdUnit == 1)
                {
                    return $"{UnitNumber(firstUnit)} {UNIT_OF_HUNDREDS} {UNIT_OF_TENS} {UnitNumber(thirdUnit)}";
                }

                if (thirdUnit == 5)
                {
                    return $"{UnitNumber(firstUnit)} {UNIT_OF_HUNDREDS} {UNIT_OF_TENS} {NUMBER_FIVE}";
                }

                return $"{UnitNumber(firstUnit)} {UNIT_OF_HUNDREDS} {NUMBER_TEN} {UnitNumber(thirdUnit)}";
            default:
                if (thirdUnit == 1)
                {
                    return
                        $"{UnitNumber(firstUnit)} {UNIT_OF_HUNDREDS} {UnitNumber(secondUnit)} {UNIT_OF_TENS} {NUMBER_ONE}";
                }

                if (thirdUnit == 0)
                {
                    return $"{UnitNumber(firstUnit)} {UNIT_OF_HUNDREDS} {UnitNumber(secondUnit)} {UNIT_OF_TENS}";
                }

                if (thirdUnit == 5)
                {
                    return $"{UnitNumber(firstUnit)} {UNIT_OF_HUNDREDS} {UnitNumber(secondUnit)} {NUMBER_FIVE}";
                }

                return
                    $"{UnitNumber(firstUnit)} {UNIT_OF_HUNDREDS} {UnitNumber(secondUnit)} {UNIT_OF_TENS} {UnitNumber(thirdUnit)}";
        }
    }

    private string UnitOfTens(int number)
    {
        string result = "";
        var character = number.ToString();
        var firstUnit = GetNumberInCharacter(character, 0);
        var secondUnit = GetNumberInCharacter(character, 1);
        switch (character[1])
        {
            case '0':
                if (character[0].Equals('1'))
                {
                    return NUMBER_TEN;
                }

                result = $"{UnitNumber(firstUnit)} {UNIT_OF_TENS}";
                return result;
            case '1':
                if (character[0].Equals('1'))
                {
                    return $"{NUMBER_TEN} {UnitNumber(secondUnit)}";
                }

                result = $"{UnitNumber(firstUnit)} {UNIT_OF_TENS} {NUMBER_ONE}";
                return result;
            case '5':
                result = $"{UnitNumber(firstUnit)} {UNIT_OF_TENS} {NUMBER_FIVE}";
                return result;
            default:
                result = $"{NUMBER_TEN} {UnitNumber(secondUnit)}";
                return result;
        }
    }

    private int GetNumberInCharacter(string character, int index)
    {
        return int.Parse(character[index].ToString());
    }

    private string UnitNumber(int number)
    {
        return number switch
        {
            1 => "một",
            2 => "hai",
            3 => "ba",
            4 => "bốn",
            5 => "năm",
            6 => "sáu",
            7 => "bảy",
            8 => "tám",
            9 => "chín",
            0 => "không",
            _ => ""
        };
    }
}